$(document).ready(function(){

	insertHideButton();

	$("a.toggle-hide").click(function(e){
		e.preventDefault();
			
		$(this).parents(".hide-group").find('.hideable').toggle("slow");
	});

	$(".hideable").hide();
	
	function insertHideButton() {
	    $(".hide-button-anchor").append('<a class="toggle-hide" href="#">Beschreibung anzeigen</a>')
	    $(".hide-button-anchor-trailer").append('<a class="toggle-hide" href="#">Trailer und Beschreibung anzeigen</a>')
	}
});