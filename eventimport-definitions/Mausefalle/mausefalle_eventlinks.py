# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import timedelta
from urlparse import urljoin
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:td[@class="event"]')
        
    def process_event_node(self, item, event_node):
        rel_url = self.html_xpath(event_node, 'h:a/@href')[0]
        abs_url = urljoin(item['page_url'], rel_url)
        item['page_url'] = abs_url