# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:div[@class="event_info"]')
        
    def process_event_node(self, item, event_node):
        
        item['title'] = self.html_xpath_text(event_node, '//h:td[@class="title"]')
        item['description'] =  self.collect_texts(event_node, '//h:td[@class="description"]')

        start_datetime_string  = self.html_xpath(event_node, '//h:td[@class="date"]/text()')[0].strip()
        end_datetime_string  = self.html_xpath(event_node, '//h:td[@class="date"]/text()')[1].strip()
        item['start'] = datetime.strptime(start_datetime_string, '%d.%m.%Y %H:%M -')
        item['end'] = datetime.strptime(end_datetime_string, '%d.%m.%Y %H:%M')