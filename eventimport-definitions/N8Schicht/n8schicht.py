# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:div[@class = "mod_eventlist teaserArea"]/h:div[@class != "empty"]')
        
    def process_event_node(self, item, event_node):
        
        item['title'] = self.html_xpath_text(event_node, 'h:div[@class="partyInfo"]//h:h3')
        item['description'] =  self.collect_texts(event_node, './/h:div[@class="partyText"]')
        
        month_day_string = self.html_xpath_text(event_node, 'h:div[@class="partyInfo"]//h:h2')
        
        m = re.search('\S*,\s(\d+).(\d+)', month_day_string)
        
        item['start'] = self.next_possible_date(month=int(m.group(2)), day=int(m.group(1)))
        item['no_starttime'] = True
        item['end'] = None