# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import time
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        grouped_screenings = self.html_xpath(self.tree, '//h:div[@class = "rex_plan" or @class = "nfb_plan"]//h:div[@class = "wochenplan_tabelle"]')
        screenings = []
        
        for screening in grouped_screenings:
            colspan = int(self.html_xpath(screening, 'ancestor::h:td[1]/@colspan')[0])
            
            for i in xrange(colspan):
                screenings.append((screening, i))
            
        return screenings
        
    def process_event_node(self, item, event_node_tuple):
        
        event_node = event_node_tuple[0]
        days_offset = event_node_tuple[1]
        
        preceding_columns = self.html_xpath(event_node, 'ancestor::h:td[1]/preceding-sibling::h:td')
        preceding_colspans = self.html_xpath(event_node, 'ancestor::h:td[1]/preceding-sibling::h:td/@colspan')
        
        column = len(preceding_columns) + sum([int(colspan) - 1 for colspan in preceding_colspans])
        
        date_string = self.collect_texts(event_node, 'ancestor::h:table//h:tr[1]/h:th[{0}]//h:span[@class = "tabelle_tage_schrift"]'.format(column+1)).strip()
        
        start_time_string = self.collect_texts(event_node, './/h:div[@class = "tabelle_filminfo"]').strip()
        
        item['start'] = datetime.strptime(date_string[-10:] + start_time_string[:5], '%d.%m.%Y%H:%M') + timedelta(days_offset)
        
        item['page_url'] = self.html_xpath(event_node, './/h:a[@class = "tabelle_filmtitel_link"]/@href')[0]