# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import time
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        
        return self.html_xpath(self.tree, './/h:div[@id = "inhalt"]')
        
    def process_event_node(self, item, event_node):
        
        movie_id = item['page_url'].split('#')[1]
        
        movie_node = self.html_xpath(event_node, './/h:div[(@class = "rex_liste" or @class = "nfb_liste") and .//h:a/@name = "{0}"]'.format(movie_id))[0]
                
        movie_title = self.collect_texts(movie_node, './/h:div[@class = "filmtitel"]').strip()
        
        item['title'] = movie_title
        item['movie_title'] = movie_title
        item['description'] = self.collect_texts(movie_node, './/h:div[@class = "filminfo"]').strip()