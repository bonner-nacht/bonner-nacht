# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher
from lxml.html import html5parser, tostring

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.tree.xpath('/rss/channel/item')
        
    def process_event_node(self, item, event_node):
        
        item['title'] = event_node.find('title').text
        item['website'] = self.html_xpath_text(event_node, 'link')
        
        desc_html_string = event_node.xpath('content:encoded', namespaces={ "content": "http://purl.org/rss/1.0/modules/content/"})[0].text
        desc_html = html5parser.fromstring(desc_html_string)
        
        item['description'] = self.html_xpath_text(desc_html, '//h:p')

        # TODO improve whitespace handling
        datetime_string = re.sub(re.compile(r'\s+'), '', self.html_xpath_text(desc_html, '//h:div[2]').replace(u'\xa0', ' ')).encode("utf8")
        
        if '–' in datetime_string:
            start_string = datetime_string.split('–')[0]
            end_string = datetime_string.split('–')[1]
            if len(start_string) > 10:
                item['start'] = datetime.strptime(start_string, '%d.%m.%Y%H:%M')
                item['end'] = datetime.strptime(end_string, '%d.%m.%Ybis%H:%M')
            else:
                item['start'] = datetime.strptime(start_string, '%d.%m.%Y')
                item['end'] = datetime.strptime(end_string, '%d.%m.%Y')
                item['no_starttime'] =  True
                item['no_endtime'] = True
        else:
            item['start'] = datetime.strptime(datetime_string.split('bis')[0], '%d.%m.%Y%H:%M')
        
            endtime = datetime.strptime(datetime_string.split('bis')[1], '%H:%M').time()
            item['end'] = self.calculate_enddatetime(item['start'], endtime)