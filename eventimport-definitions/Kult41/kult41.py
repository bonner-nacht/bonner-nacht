# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import time
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:html/h:body/h:div/h:table[@width=\'510\']/h:tbody')
        
    def process_event_node(self, item, event_node):
        desc_elem = self.html_find(event_node, 'h:tr[4]/h:td[2]/h:div/h:table/h:tbody/h:tr/h:td')
        item['title'] = self.html_find_text(desc_elem, 'h:span')
        
        item['description'] = self.collect_texts(desc_elem, 'h:br[2]/following-sibling::node()')
        
        start_date = self.html_find_text(event_node, 'h:tr[2]/h:td[1]/h:div')
        
        timerange = self.html_find_text(event_node, 'h:tr[2]/h:td[2]/h:div')
        t_m = re.search('(\d+):(\d+)(\sbis\s(\d+):(\d+))?', timerange)
        
        d_m = re.search('(\d+).(\d+).(\d+)', start_date)
        if d_m is not None and t_m is not None:
            item['start'] = datetime(int(d_m.group(3)), int(d_m.group(2)), int(d_m.group(1)), int(t_m.group(1)), int(t_m.group(2)))
            
            if t_m.group(4) is None:
                item['end'] = None
            else:
                hours = int(t_m.group(4))
                minutes = int(t_m.group(5))
                
                if hours == 24:
                    hours = 0
                
                endtime = time(hours, minutes)
                item['end'] = self.calculate_enddatetime(item['start'], endtime)