# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import time
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:div[@class = "movies-list"]/h:a')
        
    def process_event_node(self, item, event_node):
        
        title = self.html_xpath_text(event_node, './/h:div[@class = "movie-details"]/h:div[@class = "title"]')
        location = self.html_xpath_text(event_node, './/h:div[@class = "movie-details"]/h:div[@class = "location"]')
        
        # Skip the placeholder
        if title == 'Programm':
            item['drop'] = True
            return
        
        # only import events for the current location
        if location == 'Kino in der Brotfabrik':
            if self.params != 'Brotfabrik': 
                item['drop'] = True
                return
        elif location == 'LVR-LandesMuseum Bonn':
            if self.params != 'LVR': 
                item['drop'] = True
                return
        else:
            self.logger.warning('Unknown Location for Bonner Kinemathek!')
            item['drop'] = True
            return
        
        
        item['page_url'] = self.html_xpath(event_node, '@href')[0]
        start_time_string = self.html_xpath_text(event_node, './/h:div[@class = "movie-details"]/h:span[@class = "date"]')
        date_string = self.html_xpath_text(event_node, 'ancestor::h:div[@class = "movies-list"]/preceding-sibling::h:h2[1]')
        item['start'] = datetime.strptime(date_string[-10:] + start_time_string, '%d/%m/%Y%H:%M')