Screenings of BonnerKinemathek can be in two venues: 

* Kino in der Brotfabrik
* LVR Landesmuseum

They share a website for screenings of both places at http://www.bonnerkinemathek.de/ for which the import scripts are in this folder and
meta information is in the corresponding folder for each venue along with a symlink to the scripts here. 