# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import timedelta
from urlparse import urljoin
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:div[@class="content-termine"]')
        
    def process_event_node(self, item, event_node):
        
        location = self.html_xpath_text(event_node, 'h:div[@class="termin-short"]/h:h5').strip()
        
        # only import events for the current location
        if 'Pantheon Casino' in location:
            if self.params != 'Casino': 
                item['drop'] = True
                return
        elif u'Brückenforum' in location:
            if self.params != 'Brueckenforum': 
                item['drop'] = True
                return
        elif u'Beethovenhalle' in location:
            if self.params != 'Beethovenhalle': 
                item['drop'] = True
                return
        elif u'Oper Bonn' in location:
            if self.params != 'Opernhaus': 
                item['drop'] = True
                return
        else:
            if self.params != 'Pantheon':
                item['drop'] = True
                return               
        
        rel_url = self.html_xpath(event_node, './/h:a/@href')[0]
        abs_url = urljoin(item['page_url'], rel_url)
        item['page_url'] = abs_url