# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:div[@class="content-termine"]')
        
    def process_event_node(self, item, event_node):
        
        item['title'] = self.html_xpath_text(event_node, 'h:div[@class="termin-long"]/h:h3').strip()
        item['description'] =  self.collect_texts(event_node, 'h:div[@class="termin-long"]/h:div[@class="bodySlots"]/h:div[@class = "bText"]/h:p')
        
        date_string  = self.html_xpath_text(event_node, 'h:div[@class = "datum-long"]/h:h3').strip()
        
        # There are some events without date, so just return and let it be dropped by eventvalidator 
        if not "." in date_string:
            return 
        
        time_string  = self.html_xpath(event_node, 'h:div[@class = "datum-long"]/h:h5[2]/text()[2]')[0].strip()[0:5]
        
        item['start'] = datetime.strptime(date_string + time_string, '%d.%m.%y%H:%M')
        item['end'] = None