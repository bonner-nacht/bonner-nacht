# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import time
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:td[@class = "tab_trenner"]/h:a')
        
    def process_event_node(self, item, event_node):
        
        movie_content_node = self.html_xpath(event_node, 'ancestor::h:div[@class = "film_content"]')[0]
        movie_title = self.collect_texts(movie_content_node, './/h:h2')
        
        item['title'] = movie_title
        item['movie_title'] = movie_title
        
        column = int(self.html_xpath(event_node, 'count(ancestor::h:td/preceding-sibling::*)')) + 1
        
        start_date_str = self.html_xpath(event_node, 'ancestor::h:table//h:tr[1]/h:th[' + str(column) + ']')[0].text
        
        if 'Heute' in start_date_str:
            start_date = datetime.today()
        else:
            start_date = self.next_possible_date(int(start_date_str[6:8]), int(start_date_str[3:5]))
        
        start_time = self.html_xpath_text(event_node, '.').split(':')
        
        item['start'] = start_date.replace(hour = int(start_time[0]), minute = int(start_time[1]))
        item['description'] = unicode(self.html_xpath(movie_content_node, './/h:table/preceding-sibling::text()')[0])
        # Todo endtime