# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:div[@class = "mod_eventlist block"]/h:div')
        
    def process_event_node(self, item, event_node):
        
        item['title'] = self.html_xpath_text(event_node, 'h:h3')
        item['description'] = self.collect_texts(event_node, 'h:div[@class = "ce_text"]')
        
        datetime_string = self.html_xpath_text(event_node, 'h:p[@class = "info"]')
        
        date_string = datetime_string[:10]
        
        if datetime_string[11:]:
            time_string = datetime_string[-10:-5]
            item['start'] = datetime.strptime(date_string + time_string, '%d.%m.%Y%H:%M')
        else:
            item['start'] = datetime.strptime(date_string, '%d.%m.%Y')
            item['no_starttime'] = True
        
        item['end'] = None