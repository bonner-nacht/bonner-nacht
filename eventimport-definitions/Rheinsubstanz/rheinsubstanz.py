# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:section[@class = "mod_eventlist block"]/h:div[@class = "teaserElement"]')
        
    def process_event_node(self, item, event_node):
        
        item['title'] = self.html_xpath_text(event_node, './/h:h4')
        item['description'] =  self.collect_texts(event_node, './/h:p')
        
        date_string = self.html_xpath_text(event_node, './/h:span[@class="date"]')
        time_string = self.html_xpath_text(event_node, './/h:p[@class = "time"]')
        
        item['start'] = datetime.strptime(date_string + time_string, '%d.%m.%Y%H:%M')
        item['end'] = None