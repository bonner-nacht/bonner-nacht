# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:div[@id="eventlist"]')
        
    def process_event_node(self, item, event_node):
        
        item['title'] = self.html_xpath_text(event_node, '//h:dd[@class="title"]')
        item['description'] =  self.collect_texts(event_node, '//h:div[@class="description event_desc"]')
        
        #FIXME fix encoding problem
        datetime_string  = self.html_xpath_text(event_node, '//h:dd[@class="when"]').replace(u'\xa0', ' ').strip()
        if ' h' in datetime_string:
            item['start'] = datetime.strptime(datetime_string, '%d.%m.%Y %H.%M h')
        else:
            item['start'] = datetime.strptime(datetime_string, '%d.%m.%Y')
            item['no_startime'] = True
            
        item['end'] = None