# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher
    
def parse_month(string):
    return {
        'Januar': 1,
        'Februar': 2,
        # TODO FIXME
        u'März': 3,
        'April': 4,
        'Mai': 5,
        'Juni': 6,
        'Juli': 7,
        'August': 8,
        'September': 9,
        'Oktober': 10,
        'November': 11,
        'Dezember': 12,
    }[string]

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.tree.xpath('/rss/channel/item')
        
    def process_event_node(self, item, event_node):
        
        desc_text = event_node.find('description').text
        
        dt = re.search('\s([0-9]+)\.\s(\S+)\s([0-9]+);\s([0-9]+):([0-9]+);', self.substring_between(desc_text, '[', ']'))
        
        if dt:
            item['title'] = event_node.find('title').text
            item['website'] = self.html_xpath_text(event_node, 'link')
            item['description'] = self.substring_after(desc_text, '] ')
            item['start'] = datetime(int(dt.group(3)), parse_month(dt.group(2)), int(dt.group(1)), int(dt.group(4)), int(dt.group(5)))
            item['end'] =  None