# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import timedelta
from urlparse import urljoin
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:ul[@class = "event-list"]/h:li')
        
    def process_event_node(self, item, event_node):
        
        rel_url = self.html_xpath(event_node, 'h:div[@class = "event-content"]/h:h2/h:a/@href')[0]
        abs_url = urljoin(item['page_url'], rel_url)
        item['page_url'] = self.urlencode_path(abs_url)