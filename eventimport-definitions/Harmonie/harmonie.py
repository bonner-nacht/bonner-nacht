# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:ul[@class = "listeveranstaltungen listeveranstaltungen2"]/h:li')
        
    def process_event_node(self, item, event_node):
        
        item['title'] = self.html_xpath_text(event_node, 'h:span[@class = "text"]/h:h2[@class ="title"]/h:a')
        item['description'] = self.collect_texts(event_node, 'h:span[@class = "text"]/h:p[@class ="kurztext"]')
        
        year_month = self.substring_after(self.html_xpath(event_node, 'h:span[@class = "text"]/h:h2[@class ="title"]/h:a/@href')[0], 'month=')
        day = self.html_xpath_text(event_node, 'h:span[@class = "datum"]/h:span[@class = "datum1"]')
        starttime = self.html_xpath_text(event_node, 'h:span[@class = "datum"]/h:span[@class = "uhrzeit1"]')
        
        item['start'] = datetime.strptime(year_month + day + starttime, '%Y%m%d%H:%M')
        item['end'] = None