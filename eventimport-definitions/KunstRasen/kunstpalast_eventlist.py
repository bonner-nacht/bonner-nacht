# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import timedelta
from urlparse import urljoin
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:div[@id = "inner_content"]//h:table[1]//h:tr[h:td//text() = "KUNST!PALAST"]')
        
    def process_event_node(self, item, event_node):
                
        rel_url = self.html_xpath(event_node, './/h:a/@href')[0]
        abs_url = urljoin(item['page_url'], rel_url)
        
        # The following url returns 404 so we excluded it here
        # by passing an arbitary existing url without any matching content 
        if abs_url == 'http://www.kunstrasen-bonn.de/index.php/veranstaltungen/veranstaltungen-kunst-rasen/the-b-52-s':
            item['page_url'] = 'https://gitorious.org/bonner-nacht'
        else:
            item['page_url'] = abs_url
            
        item['title'] = self.html_xpath_text(event_node, './/h:a')
        
        date_string  = self.html_xpath_text(event_node, 'h:td[2]')
        time_string = self.html_xpath_text(event_node, 'h:td[4]')
        
        d_m = re.search('(\d+).(\d+).(\d+)', date_string)
        t_m= re.search('(\d+).(\d+) Uhr', time_string)
        
        # fix wrong year
        
        item['start'] = datetime(2013, int(d_m.group(2)), int(d_m.group(1)), int(t_m.group(1)), int(t_m.group(2)))
        item['end'] = None
        print(item['start'])