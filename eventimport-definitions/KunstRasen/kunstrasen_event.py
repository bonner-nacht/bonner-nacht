# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:div[@class = "item-page"]')
        
    def process_event_node(self, item, event_node):
        
        item['description'] =  self.collect_texts(event_node, 'h:p')