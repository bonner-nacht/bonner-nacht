# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import time
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        
        return self.html_xpath(self.tree, '//h:div[@class = "time_table_td"]/h:a')
        
    def process_event_node(self, item, event_node):
                
        movie_title = self.html_xpath_text(event_node, 'ancestor::h:div[@id = "main_content_full"]//h:h1').strip()
        
        item['title'] = movie_title
        item['movie_title'] = movie_title
        item['description'] = self.collect_texts(event_node, 'ancestor::h:div[@id = "main_content_full"]//h:div[@id = "fs_tab_info_content"]').strip()
        
        starttime_string = self.html_xpath_text(event_node, '.')
        
        datestring = self.collect_texts(event_node, 'ancestor::h:div[1]/preceding-sibling::h:div[1]').strip()
        startdate = self.next_possible_date(int(datestring[5:7]), int(datestring[2:4]))
        item['start'] = startdate.replace(hour = int(starttime_string[0:2]), minute = int(starttime_string[3:5]))