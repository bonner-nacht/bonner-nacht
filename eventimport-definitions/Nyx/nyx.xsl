<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:html="http://www.w3.org/1999/xhtml" xmlns:wasg="http://launchpad.net/wasgehtengine">

    <xsl:output encoding="UTF-8" indent="yes" method="xml" />

    <xsl:template match="/">
        <events>
            <xsl:for-each select="//html:div[@class = 'event']">
                <xsl:call-template name="event-template" />
            </xsl:for-each>
        </events>
    </xsl:template>

    <xsl:template name="event-template">
        <xsl:variable name="start-date">
            <xsl:call-template name="wasg:parse-date">
                <xsl:with-param name="date-string" select="preceding-sibling::html:h3[1]" />
            </xsl:call-template>
        </xsl:variable>
        <event>
            <name>
                <xsl:value-of select="html:h4" />
            </name>
            <description>
                <xsl:apply-templates select="html:p[1]" />
            </description>
            <occurrence>
                <start-date><xsl:value-of select="$start-date" /></start-date>
                <start-time><xsl:value-of select="concat(substring-before(substring-after(html:p[@class='footer'],'Beginn: '), ' Uhr'),':00')" /></start-time>
                <occurrenceType>SINGLE</occurrenceType>
            </occurrence>
        </event>
    </xsl:template>

    <xsl:template name="wasg:parse-date">
        <xsl:param name="date-string" />
        <xsl:variable name="month">
            <xsl:call-template name="wasg:parse-month">
                <xsl:with-param name="month-string" select="substring-before(substring-after($date-string, '. '), ' ')" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="concat(
            substring-after(substring-after($date-string, '. '), ' '),
            '-',
            $month,
            '-',
            substring-before(substring-after($date-string, ', '), '.'))" />
    </xsl:template>

    <xsl:template name="wasg:parse-month">
        <xsl:param name="month-string" />
        <xsl:choose>
            <xsl:when test="$month-string = 'Januar'">
                <xsl:text>01</xsl:text>
            </xsl:when>
            <xsl:when test="$month-string = 'Februar'">
                <xsl:text>02</xsl:text>
            </xsl:when>
            <xsl:when test="$month-string = 'März'">
                <xsl:text>03</xsl:text>
            </xsl:when>
            <xsl:when test="$month-string = 'April'">
                <xsl:text>04</xsl:text>
            </xsl:when>
            <xsl:when test="$month-string = 'Mai'">
                <xsl:text>05</xsl:text>
            </xsl:when>
            <xsl:when test="$month-string = 'Juni'">
                <xsl:text>06</xsl:text>
            </xsl:when>
            <xsl:when test="$month-string = 'Juli'">
                <xsl:text>07</xsl:text>
            </xsl:when>
            <xsl:when test="$month-string = 'August'">
                <xsl:text>08</xsl:text>
            </xsl:when>
            <xsl:when test="$month-string = 'September'">
                <xsl:text>09</xsl:text>
            </xsl:when>
            <xsl:when test="$month-string = 'Oktober'">
                <xsl:text>10</xsl:text>
            </xsl:when>
            <xsl:when test="$month-string = 'November'">
                <xsl:text>11</xsl:text>
            </xsl:when>
            <xsl:when test="$month-string = 'Dezember'">
                <xsl:text>12</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
