# -*- coding: utf-8 -*-
from lxml import etree
import re
from datetime import datetime
from datetime import time
from datetime import timedelta
from importlib import import_module
SemanticsMatcher = import_module('wasgehtengine.import.semanticsmatcher').SemanticsMatcher

class Parser(SemanticsMatcher):
        
    def event_node_iter(self):
        return self.html_xpath(self.tree, '//h:p[@class = "playtime"]/h:span')
        
    def process_event_node(self, item, event_node):
        
        movie_section = self.html_xpath(event_node, 'ancestor::h:div[contains(@class,"movieResultItem")]')[0]
        movie_title = self.html_xpath(movie_section, './/h:h3//text()')[0].strip()
        
        item['title'] = movie_title
        item['movie_title'] = movie_title
        item['description'] = self.collect_texts(movie_section, './/h:div[@class = "preSellingMovieTeaser"]')
        
        start_time = self.collect_texts(event_node, '.').strip().split(':')
        
        item['start'] = item['start'].replace(hour = int(start_time[0]), minute = int(start_time[1]))
        
        # Todo endtime